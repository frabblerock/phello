'use strict';

(function () {
    'use strict';

    var hamburger = {
        init: function init() {
            var toggle = document.querySelector('.hamburger-menu');
            var bar = document.querySelector('.bar');

            toggle.addEventListener('click', function () {
                bar.classList.toggle('animate');
                document.body.classList.toggle('nav-open');
            });
        }
    };

    var hero = {
        init: function init() {
            var el = document.querySelector('.hero');

            el.classList.add('scrolled');
        }
    };

    setTimeout(function () {
        hero.init();
    }, 250);

    hamburger.init();
})();
//# sourceMappingURL=main.js.map
